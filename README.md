Maven, 1 day
====

Welcome to this course.
The syllabus can be find at
[build/maven](https://www.ribomation.se/build/maven.html)

Here you will find
* Installation instructions
* Solutions to the programming exercises
* Source code to the sample project

Usage
====

You need to have a GIT client installed to clone this repo. Otherwise, you can just click on the download button and grab it all as a ZIP or TAR bundle.

* [GIT Client Download](https://git-scm.com/downloads)

Get the sources initially by a git clone operation
    
    git clone https://gitlab.com/ribomation-courses/build-tools/maven.git
    cd maven

Get the latest updates by a git pull operation

    git pull

Installation Instructions
====

In order to do the programming exercises of the course, you need to have
Java JDK installed. In addition, you need to install Maven and be using a decent Java IDE. Finally, you need to install the Nexus server.

* [Java 8 JDK Download](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
* [Maven Download](https://maven.apache.org/download.cgi)
* [MS Visual Code](https://code.visualstudio.com/download)
* [JetBrains IntellJ IDEA](https://www.jetbrains.com/idea/download)
* [Nexus Maven Repository](https://www.sonatype.com/download-oss-sonatype)

***
*If you have any questions, don't hesitate to contact me*<br>
**Jens Riboe**<br/>
Ribomation AB<br/>
[jens.riboe@ribomation.se](mailto:jens.riboe@ribomation.se)<br/>
[www.ribomation.se](https://www.ribomation.se)<br/>

