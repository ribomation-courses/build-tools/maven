package fmt;

import org.simpleframework.xml.core.Persister;
import result.Result;

import java.io.PrintWriter;
import java.io.Writer;
import java.util.List;

/**
 * Outputs in XML format
 */
public class XmlFormatter implements Formatter {

    @Override
    public void format(List<Result> results, Writer out) {
        try {
            PrintWriter pw = new PrintWriter(out);
            pw.printf("<results>%n");
            Persister persister = new Persister();
            for (Result r : results) {
                persister.write(r, out);
                pw.printf("%n");
            }
            pw.printf("</results>%n");
            pw.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
