package result;

import java.math.BigInteger;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Encapsulates the result for one argument
 *
 * @user jens
 * @date 2015-05-03
 */
public class Result {
    private Integer            argument = 0;
    private Map<String, Value> values   = new LinkedHashMap<>();


    public Result(Integer argument) {
        this.argument = argument;
    }

    public void add(String name, BigInteger result, double elapsed) {
        values.put(name, new Value(name, result, elapsed));
    }

    public Integer getArgument() {
        return argument;
    }

    public Map<String, Value> getValues() {
        return values;
    }


}
