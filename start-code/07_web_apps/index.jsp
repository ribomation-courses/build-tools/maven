<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page import="model.Model" %>
<%@ page import="result.Result" %>
<%@ page import="functions.Sum" %>
<%@ page import="functions.Fibonacci" %>
<%@ page import="functions.Factorial" %>
<%
    int arg = 10;
    if (request.getParameter("arg") != null) {
        arg = Integer.parseInt(request.getParameter("arg"));
    }

    Model model = new Model();
    model.addFunction(new Sum());
    model.addFunction(new Fibonacci());
    model.addFunction(new Factorial());
    model.setArgument(arg);
    model.compute();

    Result result = model.getResults().iterator().next();
    request.setAttribute("result", result);
%>
<!doctype html>
<html lang="en">
<head>
    <title>Hello Web</title>
    <link rel="stylesheet" href="css/style.css">
</head>

<body>
<div id="content">
    <h1>Functions</h1>

    <form method="get">
        <div>
            <label for="arg"> Argument: </label>
            <input name="arg" id="arg" value="<%= arg %>" placeholder="Positive integer" required/>
            <input type="submit" value="Update"/>
        </div>
    </form>

    <c:forEach items="${result.values}" var="entry">
        <div class="function">
            <h2>${entry.key}</h2>
            <table>
                <tr>
                    <th>Value</th>
                    <td class="value">${entry.value.result}</td>
                <tr>
                    <th>Elapsed</th>
                    <td>${entry.value.elapsedMilliSecs} ms</td>
                </tr>
            </table>
            <img src="img/${fn:toLowerCase(entry.key)}.png"/>
        </div>

    </c:forEach>
</div>
</body>
</html>
