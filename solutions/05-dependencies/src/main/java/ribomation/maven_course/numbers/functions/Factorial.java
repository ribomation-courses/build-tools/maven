package ribomation.maven_course.numbers.functions;

import ribomation.maven_course.numbers.functions.CachingFunction;

import java.math.BigInteger;

/**
 * Computes the <i>n!</i>.
 *
 * @see <a href="http://en.wikipedia.org/wiki/Factorial">Factorial @ WikiPedia</a>
 */
public class Factorial extends CachingFunction {
    @Override
    protected BigInteger computeResult(Integer arg) {
        if (arg <= 1) return BigInteger.ONE;
        BigInteger n1 = compute(arg - 1);
        BigInteger n  = BigInteger.valueOf(arg);
        return n.multiply(n1);
    }
}
