package ribomation.maven_course.numbers.result;

import java.math.BigInteger;

/**
 * Encapsulates a single timed function value
 *
 * @user jens
 * @date 2015-05-03
 */
public class Value {
    public String     name;
    public BigInteger result;
    public double     elapsedMilliSecs;

    public Value(String name, BigInteger result, double elapsedMilliSecs) {
        this.name = name;
        this.result = result;
        this.elapsedMilliSecs = elapsedMilliSecs;
    }

    public String getName() {
        return name;
    }

    public BigInteger getResult() {
        return result;
    }

    public double getElapsedMilliSecs() {
        return elapsedMilliSecs;
    }


}
