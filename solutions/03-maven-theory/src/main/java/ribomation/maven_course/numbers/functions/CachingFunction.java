package ribomation.maven_course.numbers.functions;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

/**
 * Abstract base class that provides caching functionality.
 */
public abstract class CachingFunction implements Function {
    private Map<Integer, BigInteger> cache = new HashMap<>();

    /**
     * Returns the function name
     * @return its name
     */
    @Override
    public String getName() {
        return this.getClass().getSimpleName();
    }

    /**
     * Computes the function value by first checing the cache
     * for an already computed result.
     * @param arg   argument
     * @return f(x)
     */
    @Override
    public final BigInteger compute(int arg) {
        if (hasEntry(arg)) return get(arg);
        BigInteger result = computeResult(arg);
        return put(arg, result);
    }

    /**
     * Computes the raw result. Sub-classes must implement this function
     * @param arg argument
     * @return f(x)
     */
    protected abstract BigInteger computeResult(Integer arg);

    /**
     * Returns true if the cache has an entry with the given key
     * @param argument
     * @return true if in the cache
     */
    protected boolean hasEntry(Integer argument) {
        return cache.containsKey(argument);
    }

    /**
     * Returns the cache result or null.
     * @param argument  key
     * @return resuklt or null
     */
    protected BigInteger get(Integer argument) {
        return cache.get(argument);
    }

    /**
     * Inserts an argument/result com�bo
     * @param arg       argument
     * @param value     result
     * @return result
     */
    protected BigInteger put(Integer arg, BigInteger value) {
        cache.put(arg, value);
        return value;
    }

    /**
     * Returns the cache size
     * @return its cache
     */
    protected int   size() {
        return cache.size();
    }

    /**
     * Clears the cache
     */
    public void clear() {
        cache.clear();
    }

    @Override
    public String toString() {
        return getName() + "(x)";
    }
}
