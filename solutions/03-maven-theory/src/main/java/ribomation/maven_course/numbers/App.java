package ribomation.maven_course.numbers;

import ribomation.maven_course.numbers.fmt.CsvFormatter;
import ribomation.maven_course.numbers.functions.Factorial;
import ribomation.maven_course.numbers.functions.Fibonacci;
import ribomation.maven_course.numbers.functions.Sum;

import java.io.OutputStreamWriter;

/**
 * Main entry point for the CLI version.
 *
 * @user jens
 * @date 2015-05-03
 */
public class App {
    public static void main(String[] args) {
        App app = new App();
        app.parseArgs(args);
        Model model = app.init();
        app.run(model);
    }

    private int     argument     = 10;
    private boolean tabulate     = false;
    private boolean csv          = false;
    private String  csvDelimiter = ";";


    private void parseArgs(String[] args) {
        for (int k = 0; k < args.length; k++) {
            String arg = args[k];
            if (arg.startsWith("-n")) {
                argument = Integer.parseInt(args[++k]);
            } else if (arg.startsWith("-t")) {
                tabulate = true;
            } else if (arg.startsWith("-c")) {
                csv = true;
            } else if (arg.startsWith("-d")) {
                csvDelimiter = args[++k];
            } else {
                System.err.printf("Unknown option: %s%n", arg);
                System.err.printf("Options: [-number <int>] [-tabulate] [-csv] [-delimiter <char>] %n");
            }
        }
    }

    private Model init() {
        Model model = new Model(argument, tabulate);

        model.addFunction(new Sum());
        model.addFunction(new Fibonacci());
        model.addFunction(new Factorial());

        if (csv) {
            model.setFormatter(new CsvFormatter(csvDelimiter));
        }

        return model;
    }

    private void run(Model model) {
        model.compute();

        model.print(new OutputStreamWriter(System.out));
    }

}
