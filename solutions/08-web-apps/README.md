Web Apps
====

This multi-project directory is the same the previous one, except there is one more
sub-project, creating a web-app WAR file.

Build
----

First, install all sub-projects into the local cache, by in the root directory run

    mvn install

Execute
----

In order to run the web application (`web/`) using Maven, we are using the Jetty plugin, with
the following configuration

    <plugin>
        <groupId>org.eclipse.jetty</groupId>
        <artifactId>jetty-maven-plugin</artifactId>
        <version>9.4.6.v20170531</version>
        <configuration>
            <httpConnector>
                <port>9000</port>
            </httpConnector>
            <scanIntervalSeconds>10</scanIntervalSeconds>
            <webApp>
                <contextPath>/numbers</contextPath>
            </webApp>
        </configuration>
    </plugin>

Above, we have changed the http port to 9000, instead of the default of 8080.
To build the WAR file and run it using an embedded Jetty servlet container, run

    cd web
    mvn jetty:run

Then open a browser and navigate to [http://localhost:9000/numbers](http://localhost:9000/numbers)

More info about the Jetty plugin can be found at the web site of the
[Jetty plugin](https://www.eclipse.org/jetty/documentation/current/jetty-maven-plugin.html)


