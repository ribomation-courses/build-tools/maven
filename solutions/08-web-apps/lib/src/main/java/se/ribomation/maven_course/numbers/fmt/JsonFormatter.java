package se.ribomation.maven_course.numbers.fmt;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import se.ribomation.maven_course.numbers.result.Result;

import java.io.IOException;
import java.io.Writer;
import java.util.List;

/**
 * Outputs in JSON format
 */
public class JsonFormatter implements Formatter {

    @Override
    public void format(List<Result> results, Writer out) {
        try {
            Gson gson = new GsonBuilder()
                    .setPrettyPrinting()
                    .create();
            String json = gson.toJson(results);
            out.write(json);
            out.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
