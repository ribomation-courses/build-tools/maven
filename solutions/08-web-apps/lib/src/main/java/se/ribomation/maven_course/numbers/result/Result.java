package se.ribomation.maven_course.numbers.result;

import java.math.BigInteger;
import java.util.LinkedHashMap;
import java.util.Map;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementMap;
import org.simpleframework.xml.Root;


/**
 * Encapsulates the result for one argument
 *
 * @user jens
 * @date 2015-05-03
 */
@Root(name = "computation")
public class Result {
    @Attribute
    private Integer            argument = 0;

    @ElementMap(entry = "function", key = "name", attribute = true, inline = true)
    private Map<String, Value> values   = new LinkedHashMap<>();


    public Result(Integer argument) {
        this.argument = argument;
    }

    public void add(String name, BigInteger result, double elapsed) {
        values.put(name, new Value(name, result, elapsed));
    }

    public Integer getArgument() {
        return argument;
    }

    public Map<String, Value> getValues() {
        return values;
    }

}
