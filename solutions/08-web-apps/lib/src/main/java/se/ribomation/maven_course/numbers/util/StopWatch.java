package se.ribomation.maven_course.numbers.util;

/**
 * Time a computation
 *
 * @user jens
 * @date 2015-05-03
 */
public class StopWatch {
    private long startTime, stopTime;
    
    public interface Expr<T> {
        T compute();
    }

    public StopWatch() {
        startTime = stopTime = System.nanoTime();
    }
    
    public StopWatch start() {
        startTime = System.nanoTime();
        return this;
    }
    
    public StopWatch stop() {
        stopTime = System.nanoTime();
        return this;
    }

    public long elapsed() {
        return stopTime - startTime;
    }

    public double elapsedMilliSecs() {
        return elapsed() * 1E-6;
    }
    
    public double elapsedSecs() {
        return elapsed() * 1E-9;
    }

    public <T> T time(Expr<T> expression) {
        start();
        try {
            return expression.compute();
        } finally {
            stop();
        }
    }
    
}
