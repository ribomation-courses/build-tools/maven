package se.ribomation.maven_course.numbers.functions;

import org.junit.Before;
import org.junit.Test;

import java.math.BigInteger;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Unit test of function fibonacci
 *
 * @user jens
 * @date 2015-05-02
 */
public class FibonacciTest {
    private Fibonacci target;

    @Before
    public void setUp() throws Exception {
        target = new Fibonacci();
    }

    @Test
    public void testComputeResult_10() throws Exception {
        assertThat(target.computeResult(10), is(BigInteger.valueOf(55)));
    }

    @Test
    public void testComputeResult_42() throws Exception {
        assertThat(target.computeResult(42), is(BigInteger.valueOf(267_914_296)));
    }

    @Test
    public void testComputeResult_100() throws Exception {
        assertThat(target.computeResult(100), is(new BigInteger("354224848179261915075")));
    }
}
