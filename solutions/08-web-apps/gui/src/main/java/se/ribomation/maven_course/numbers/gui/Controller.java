package se.ribomation.maven_course.numbers.gui;


import se.ribomation.maven_course.numbers.fmt.CsvFormatter;
import se.ribomation.maven_course.numbers.fmt.JsonFormatter;
import se.ribomation.maven_course.numbers.fmt.SimpleFormatter;
import se.ribomation.maven_course.numbers.fmt.XmlFormatter;
import se.ribomation.maven_course.numbers.model.Model;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.StringWriter;

/**
 * Bridge between UI and Model
 */
public class Controller implements ActionListener {
    GUI   gui;
    Model model;

    public Controller(GUI gui, Model model) {
        this.gui = gui;
        this.model = model;

        gui.inputPane.compute.addActionListener(this);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            model.setArgument(Integer.parseInt(gui.inputPane.input.getText()));
            model.setTabulate(gui.configPane.tabular.isSelected());

            if (gui.configPane.csv.isSelected()) {
                model.setFormatter(new CsvFormatter(";"));
            } else if (gui.configPane.json.isSelected()) {
                model.setFormatter(new JsonFormatter());
            } else if (gui.configPane.plain.isSelected()) {
                model.setFormatter(new SimpleFormatter());
            } else if (gui.configPane.xml.isSelected()) {
                model.setFormatter(new XmlFormatter());
            }

            model.clear();
            model.compute();

            StringWriter buf = new StringWriter(1000);
            model.print(buf);
            gui.outputPane.output.setText(buf.toString());
        } catch (NumberFormatException x) {
            gui.outputPane.output.setText(x.toString());
        }
    }

}
