package ribomation.maven_course.numbers.functions;

import org.junit.Before;
import org.junit.Test;

import java.math.BigInteger;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Unit test of function sum
 *
 * @user jens
 * @date 2015-05-02
 */
public class SumTest {
    private Sum target;

    @Before
    public void setUp() throws Exception {
        target = new Sum();
    }

    @Test
    public void testComputeResult_10() throws Exception {
        assertThat(target.computeResult(10), is(BigInteger.valueOf(55)));
    }

    @Test
    public void testComputeResult_100() throws Exception {
        assertThat(target.computeResult(100), is(BigInteger.valueOf(5050)));
    }
    
    @Test
    public void testComputeResult_10000() throws Exception {
        assertThat(target.computeResult(10_000), is(BigInteger.valueOf(50_005_000)));
    }
    
}
