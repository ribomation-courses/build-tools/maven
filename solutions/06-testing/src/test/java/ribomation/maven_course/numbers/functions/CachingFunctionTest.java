package ribomation.maven_course.numbers.functions;

import ribomation.maven_course.numbers.functions.CachingFunction;
import org.junit.Before;
import org.junit.Test;

import java.math.BigInteger;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.*;

/**
 * Unit test of caching function
 *
 * @user jens
 * @date 2015-05-03
 */
public class CachingFunctionTest {
    private CachingFunction target;

    @Before
    public void setUp() throws Exception {
        target = new CachingFunction() {
            @Override
            protected BigInteger computeResult(Integer arg) {
                return BigInteger.valueOf(42);
            }

            @Override
            public String getName() {
                return "foobar";
            }
        };
    }

    @Test
    public void testSize() throws Exception {
        assertThat(target.size(), is(0));
        
        target.put(1, BigInteger.ONE);
        assertThat(target.size(), is(1));
        
        target.put(10, BigInteger.TEN);
        assertThat(target.size(), is(2));
    }

    @Test
    public void testHasEntry() throws Exception {
        assertThat(target.hasEntry(10), is(false));
        target.put(10, BigInteger.TEN);
        assertThat(target.hasEntry(10), is(true));
    }

    @Test
    public void testGet() throws Exception {
        assertThat(target.get(10), nullValue());
        target.put(10, BigInteger.TEN);
        assertThat(target.get(10), notNullValue());
    }

    @Test
    public void testPut() throws Exception {
        assertThat(target.size(), is(0));
        
        target.put(10, BigInteger.TEN);
        assertThat(target.size(), is(1));
        
        target.put(10, BigInteger.TEN);
        assertThat(target.size(), is(1));
    }

    @Test
    public void testClear() throws Exception {
        assertThat(target.size(), is(0));

        target.put(0, BigInteger.ZERO);
        target.put(1, BigInteger.ONE);
        target.put(10, BigInteger.TEN);
        assertThat(target.size(), is(3));

        target.clear();
        assertThat(target.size(), is(0));
    }

    @Test
    public void testGetName() throws Exception {
        assertThat(target.getName(), is("foobar"));
    }

    @Test
    public void testCompute() throws Exception {
        assertThat(target.compute(1), is(BigInteger.valueOf(42)));
        assertThat(target.compute(10), is(BigInteger.valueOf(42)));
        assertThat(target.compute(100), is(BigInteger.valueOf(42)));
        assertThat(target.compute(1000), is(BigInteger.valueOf(42)));
    }
}