package ribomation.maven_course.numbers.functions;

import org.junit.Before;
import org.junit.Test;
import ribomation.maven_course.numbers.result.Result;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

/**
 * Unit test for result
 *
 * @user jens
 * @date 2015-05-04
 */
public class ResultTest {
    private Result         target;
    private List<Function> functions;

    @Before
    public void setUp() throws Exception {
        target = new Result(0);

        functions = new ArrayList<>();
        functions.add(new CachingFunction() {
            @Override
            public String getName() {
                return "foo";
            }

            @Override
            protected BigInteger computeResult(Integer arg) {
                return BigInteger.valueOf(42);
            }
        });
    }

    @Test
    public void testAdd() throws Exception {
        assertThat(target.getValues().size(), is(0));

        target.add("foo", BigInteger.TEN, 0);
        assertThat(target.getValues().size(), is(1));
    }


}
