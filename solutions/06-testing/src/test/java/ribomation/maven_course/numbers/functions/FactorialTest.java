package ribomation.maven_course.numbers.functions;

import ribomation.maven_course.numbers.functions.Factorial;
import org.junit.Before;
import org.junit.Test;

import java.math.BigInteger;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

/**
 * Unit test of function factorial
 *
 * @user jens
 * @date 2015-05-04
 */
public class FactorialTest {
    private Factorial target;

    @Before
    public void setUp() throws Exception {
        target = new Factorial();
    }

    @Test
    public void testComputeResult_1() throws Exception {
        assertThat(target.computeResult(1), is(BigInteger.valueOf(1)));
    }

    @Test
    public void testComputeResult_5() throws Exception {
        assertThat(target.computeResult(5), is(BigInteger.valueOf(120)));
    }

    @Test
    public void testComputeResult_100() throws Exception {
        assertThat(target.computeResult(100), is(new BigInteger("93326215443944152681699238856266700490715968264381621468592963895217599993229915608941463976156518286253697920827223758251185210916864000000000000000000000000")));
    }
    
}