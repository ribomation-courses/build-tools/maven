package se.ribomation.maven_course.numbers.gui;


import se.ribomation.maven_course.numbers.model.ResultsFormat;

import javax.swing.*;
import java.awt.*;

/**
 * Encapsulated the GUI part
 */
public class GUI extends JFrame {
    InputPane  inputPane;
    OutputPane outputPane;
    ConfigPane configPane;


    public GUI() {
        this.setTitle("Numbers - Swing GUI");
        this.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        this.setLayout(new BorderLayout());

        inputPane = new InputPane();
        outputPane = new OutputPane();
        configPane = new ConfigPane();

        this.add(inputPane, BorderLayout.NORTH);
        this.add(outputPane, BorderLayout.CENTER);
        this.add(configPane, BorderLayout.SOUTH);
    }


    class InputPane extends JPanel {
        JTextField input   = new JTextField();
        JButton    compute = new JButton("COMPUTE");

        public InputPane() {
            super(new BorderLayout());

            input.setFont(new Font("Monospaced", Font.PLAIN, 30));
            input.setText(String.valueOf(10));

            this.add(new JLabel("Argument: "), BorderLayout.WEST);
            this.add(input, BorderLayout.CENTER);
            this.add(compute, BorderLayout.EAST);
        }
    }

    class OutputPane extends JPanel {
        JTextArea output = new JTextArea();

        public OutputPane() {
            super(new BorderLayout());

            output.setBorder(BorderFactory.createLoweredBevelBorder());
            output.setFont(new Font("Monospaced", Font.PLAIN, 20));
            output.setLineWrap(true);
            output.setWrapStyleWord(false);
            output.setEditable(false);

            JScrollPane scroll = new JScrollPane(output);
            scroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

            this.add(scroll, BorderLayout.CENTER);
        }
    }

    class ConfigPane extends JPanel {
        JCheckBox    tabular = new JCheckBox("Tabular");
        JRadioButton csv     = mk(ResultsFormat.csv);
        JRadioButton json    = mk(ResultsFormat.json);
        JRadioButton plain   = mk(ResultsFormat.plain);
        JRadioButton xml     = mk(ResultsFormat.xml);

        public ConfigPane() {
            super(new FlowLayout(FlowLayout.CENTER));

            ButtonGroup group = new ButtonGroup();
            group.add(plain);
            group.add(xml);
            group.add(json);
            group.add(csv);

            this.add(tabular);
            this.add(csv);
            this.add(json);
            this.add(plain);
            this.add(xml);

            plain.setSelected(true);
            tabular.setSelected(true);
        }

        private JRadioButton mk(ResultsFormat fmt) {
            return new JRadioButton(fmt.name().toUpperCase());
        }
    }

}
