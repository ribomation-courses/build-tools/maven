package se.ribomation.maven_course.numbers.functions;

import java.math.BigInteger;

/**
 * Computes the n:th Fibonacci number.
 * @see <a href="http://en.wikipedia.org/wiki/Fibonacci_number">Fibonacci Numbers @ WikiPedia</a>
 */
public class Fibonacci extends CachingFunction {
    @Override
    protected BigInteger computeResult(Integer arg) {
        if (arg <= 2) return BigInteger.ONE;
        BigInteger f_n2 = compute(arg - 2);
        BigInteger f_n1 = compute(arg - 1);
        return f_n1.add(f_n2);
    }
}
