package se.ribomation.maven_course.numbers.fmt;


import se.ribomation.maven_course.numbers.result.Result;
import se.ribomation.maven_course.numbers.result.Value;

import java.io.PrintWriter;
import java.io.Writer;
import java.math.BigInteger;
import java.util.List;

/**
 * Output results in CSV format
 */
public class CsvFormatter implements Formatter {
    private String delimiter;

    public CsvFormatter() {
        this(";");
    }

    public CsvFormatter(String delimiter) {
        this.delimiter = delimiter;
    }

    @Override
    public void format(List<Result> results, Writer out) {
        PrintWriter pw = new PrintWriter(out, false);
        header(results.get(0), pw);
        for (Result r : results) format(r, pw);
        pw.flush();
    }

    private void header(Result result, PrintWriter out) {
        out.printf("#Input");
        for (String functionName : result.getValues().keySet()) {
            out.printf("%s%s%sms", delimiter, functionName, delimiter);
        }
        out.println();
    }

    private void format(Result result, PrintWriter out) {
        out.printf("%d", result.getArgument());

        for (Value v : result.getValues().values()) {
            BigInteger value   = v.getResult();
            double     elapsed = v.getElapsedMilliSecs();

            out.printf("%s%s%s%.3f", delimiter, value, delimiter, elapsed);
        }

        out.println();

    }
}
