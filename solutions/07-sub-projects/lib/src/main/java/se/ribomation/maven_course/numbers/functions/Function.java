package se.ribomation.maven_course.numbers.functions;

import java.math.BigInteger;

/**
 * Represents a computing function: <i>f(x)</i>
 *
 * @user jens
 * @date 2015-05-03
 */
public interface Function {

    /**
     * Computes the result given the argument
     * @param arg   argument
     * @return its result
     */
    BigInteger compute(int arg);

    /**
     * Returns its function name
     * @return its name
     */
    String getName();

    /**
     * Clears any cached data
     */
    void clear();
}
