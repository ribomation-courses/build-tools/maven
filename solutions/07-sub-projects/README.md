Sub-Projects
====

This project illustrates maven with several subprojects
* _numbers-lib_: which contains the common code
* _numbers-cli_: which is corresponds to the previous single-project application
* _numbers-gui_: a new part, containing a java Swing GUI

Build
----

You build the whole project using the usual command, from the root dir

    mvn package

This will build all artifacts in the target dir of each sub-project.

However, as you soon will discover, this is not sufficient for a multi-project application the moment
you decide to run one the applications or build inside one of the sub-projects.

The reason, is that Maven has trouble locating the dependent JARS from sibling projects. This means
that in practice you install to the local cache instead, using the following command in the root project.

    mvn install 


Execution
----
Running the CLI or GUI application, still requires all dependencies and the LIB component. Ensure 
you have successfully run `mvn install`. Then go for either the Exec plugin or create an überjar 
using the Shade plugin.

### CLI
To run the CLI application using the Exec plugin, use the following config

    <properties>
        <mainClass>se.ribomation.maven_course.numbers.cli.App</mainClass>
    </properties>
    <plugin>
        <groupId>org.codehaus.mojo</groupId>
        <artifactId>exec-maven-plugin</artifactId>
        <version>1.6.0</version>
        <configuration>
            <mainClass>${mainClass}</mainClass>
            <commandlineArgs>
                -number 45 -xml
            </commandlineArgs>
        </configuration>
    </plugin>

Then, change into the `cli/` directory and run the app via the plugin

    cd cli
    mvn exec:java

### GUI
To run the Swing GUI application using the Exec plugin, use the following config

    <properties>
        <mainClass>se.ribomation.maven_course.numbers.gui.App</mainClass>
    </properties>
    <plugin>
        <groupId>org.codehaus.mojo</groupId>
        <artifactId>exec-maven-plugin</artifactId>
        <version>1.6.0</version>
        <configuration>
            <mainClass>${mainClass}</mainClass>
        </configuration>
    </plugin>

Then, from the root dir change into the `gui/` directory and run the app via the plugin
                                             
     cd gui
     mvn exec:java

This will launch a standalone Swing window, where you can play around with different settings and formats.

