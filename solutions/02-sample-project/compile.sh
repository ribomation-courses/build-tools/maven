#!/bin/bash
set -e
set -x

SRC=./src
CLS=./classes
COMPILE="javac -d $CLS -cp $CLS"

rm -rf $CLS
mkdir -p $CLS
$COMPILE $SRC/util/*.java
$COMPILE $SRC/functions/*.java
$COMPILE $SRC/result/*.java
$COMPILE $SRC/fmt/*.java
$COMPILE $SRC/*.java
