package se.ribomation.maven_course.numbers.model;


import se.ribomation.maven_course.numbers.fmt.Formatter;
import se.ribomation.maven_course.numbers.fmt.SimpleFormatter;
import se.ribomation.maven_course.numbers.functions.Function;
import se.ribomation.maven_course.numbers.result.Result;
import se.ribomation.maven_course.numbers.util.StopWatch;

import java.io.Writer;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

/**
 * Logical model for this library.
 *
 * @user jens
 * @date 2015-05-03
 */
public class Model {
    private int     argument;
    private boolean tabulate;
    private List<Function> functions = new ArrayList<>();
    private List<Result>   results   = new ArrayList<>();
    private Formatter      formatter = new SimpleFormatter();

    public Model(int argument, boolean tabulate) {
        this.argument = argument;
        this.tabulate = tabulate;
    }

    public Model() {

    }

    public void setArgument(int argument) {
        this.argument = argument;
    }

    public void setTabulate(boolean tabulate) {
        this.tabulate = tabulate;
    }

    public void addFunction(Function f) {
        functions.add(f);
    }

    public void setFormatter(Formatter formatter) {
        this.formatter = formatter;
    }

    public void compute() {
        if (tabulate) {
            for (int k = 1; k <= argument; ++k) {
                Result result = compute(k);
                results.add(result);
            }
        } else {
            Result result = compute(argument);
            results.add(result);
        }
    }

    public Result compute(final Integer argument) {
        Result result = new Result(argument);

        for (final Function f : functions) {
            StopWatch sw = new StopWatch();
            BigInteger value = sw.time(new StopWatch.Expr<BigInteger>() {
                @Override
                public BigInteger compute() {
                    return f.compute(argument);
                }
            });
            result.add(f.getName(), value, sw.elapsedMilliSecs());
        }

        return result;
    }


    public void print(Writer out) {
        formatter.format(results, out);
    }

    public void clear() {
        results.clear();
    }

    public List<Function> getFunctions() {
        return functions;
    }

    public List<Result> getResults() {
        return results;
    }

}
