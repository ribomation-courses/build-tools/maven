package se.ribomation.maven_course.numbers.fmt;


import se.ribomation.maven_course.numbers.result.Result;
import se.ribomation.maven_course.numbers.result.Value;

import java.io.PrintWriter;
import java.io.Writer;
import java.math.BigInteger;
import java.util.List;
import java.util.Map;

/**
 * Simple textual format
 */
public class SimpleFormatter implements Formatter {

    @Override
    public void format(List<Result> results, Writer out) {
        PrintWriter pw = new PrintWriter(out, false);
        for (Result r : results) format(r, pw);
        pw.flush();
    }

    private void format(Result result, PrintWriter out) {
        out.printf("input=%d: ", result.getArgument());

        boolean first = true;
        for (Map.Entry<String, Value> e : result.getValues().entrySet()) {
            String     functionName = e.getKey();
            BigInteger value        = e.getValue().getResult();
            double     elapsed      = e.getValue().getElapsedMilliSecs();

            out.printf("%s%s=%s (%.3f ms)", (first ? "" : ", "), functionName, value, elapsed);
            first = false;
        }

        out.println();
    }

}
