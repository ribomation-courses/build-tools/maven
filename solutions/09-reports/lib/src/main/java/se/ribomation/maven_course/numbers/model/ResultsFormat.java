package se.ribomation.maven_course.numbers.model;

/**
 * Output format
 *
 * @user jens
 * @date 2015-05-04
 */
public enum ResultsFormat {
    plain, xml, json, csv
}
