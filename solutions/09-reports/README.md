Reports
====

This multi-project directory is the same the previous one, with added support for generating a
complete docs web site.

Build
----
First, install artifacts to the local cache

    mvn install


Generating a Site
----
Then, generate the site for each sub-project and aggregate them together.
Finally, the reult can be found in `target/staging/index.html`
Run the thr following in the root dir

    mvn clean install site site:stage
    firefox target/staging/index.html

