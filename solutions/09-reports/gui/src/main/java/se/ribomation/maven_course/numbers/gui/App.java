package se.ribomation.maven_course.numbers.gui;


import se.ribomation.maven_course.numbers.functions.Factorial;
import se.ribomation.maven_course.numbers.functions.Fibonacci;
import se.ribomation.maven_course.numbers.functions.Sum;
import se.ribomation.maven_course.numbers.model.Model;

/**
 * Main entry point for the GUI version.
 *
 * @user jens
 * @date 2015-05-04
 */
public class App {
    public static void main(String[] args) {
        App app = new App();
        app.setupModel();
        app.setupUI();
        app.run();
    }

    GUI        gui;
    Controller controller;
    Model      model;

    void setupUI() {
        gui = new GUI();
        controller = new Controller(gui, model);
    }

    void setupModel() {
        model = new Model();
        model.addFunction(new Sum());
        model.addFunction(new Fibonacci());
        model.addFunction(new Factorial());
    }

    private void run() {
        gui.setSize(800, 500);
        gui.setLocationByPlatform(true);
        gui.setVisible(true);
    }

}
